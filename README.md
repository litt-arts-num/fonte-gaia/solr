Pour installer Solr sur un site Omeka, il faut installer un répertoire solr en plus du plugin.    
Ce projet contient le répertoire solr nécessaire.    
Pour commencer il faut installer java sur le serveur à l'aide de cette ligne de commande:   
`sudo apt install openjdk-11-jre-headless`   
Puis vous pouvez vérifier la bonne installation de java et sa version:   
`java -version`   
Après l'installation de Java, vous pouvez faire un git clone de ce projet (au même niveau que votre installation omeka):   
`git clone git@gitlab.com:litt-arts-num/fonte-gaia/solr.git`   
En plus de cela, il faudra installer via un `git pull` le plugin SolR. Si vous faites un `git pull` de Fonte Gaia Bib C, le plugin sera installé.   
Puis il vous faudra lancer solr :   
`bin/solr start -m 2g –v`   
Cette commande en plus de lancer solr lui rajoute 2G de mémoire vive. (On ne sait pas encore si c'est nécessaire) 
Il faut créer le lien entre solr et omeka, pour cela il faut modifier le fichier de configuration de votre site :     
`sudo nano /etc/apache2/sites-available/nom-de-fichier.conf`    
Nous avons ajouter les lignes suivantes afin de faire le lien entre omeka et solr:    
        `ProxyPass /solr/ http://localhost:8983/solr/`    
        `ProxyPassReverse /solr/ http://localhost:8983/solr/`   
        `ProxyPass / http://localhost:8901/`   
        `ProxyPassReverse / http://localhost:8901/`    
Après cela il vous faudra configurer SolrSearch sur votre site Omeka.   